package com.project.terabit.utility;

import java.util.ArrayList;



/**
 * The Class PasswordHashing.
 */
public class PasswordHashing {
       
       /**
        * Hashing.
        *
        * @param password the password
        * @return the string
        */
       public String hashing(String password) {
       ArrayList<Character> individualcharacterlist=new ArrayList<>();
       Integer hashcode=0;
       Integer initialvalue=1;
       ArrayList<Integer> powers= new ArrayList<>();
       for(int j=0;j<password.length();j++) {
              initialvalue=initialvalue*password.length();
              powers.add(initialvalue);
       }
       for(int index=0;index<password.length();index++) {
              Character individualcharacter=password.charAt(index);
              individualcharacterlist.add(individualcharacter);    
       }
       for(int index=0;index<password.length();index++) {
              if(index%2==0) {
                     hashcode=hashcode+individualcharacterlist.get(password.length()-1-index).hashCode()*powers.get(password.length()-1-index)+(13*index)-(5*(index-1))+(7*(index+18));
              }
              if(index%2==1) {
                     hashcode=hashcode+individualcharacterlist.get(password.length()-1-index).hashCode()*powers.get(password.length()-1-index)+(13*index)-(5*(index-1))+(7*(index+18));
              }
       }
              return hashcode.toString();
       }

	
}