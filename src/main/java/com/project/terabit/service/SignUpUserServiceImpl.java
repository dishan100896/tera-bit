package com.project.terabit.service;

import java.time.LocalDateTime;

import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.project.terabit.entity.UserEntity;
import com.project.terabit.model.User;
import com.project.terabit.repositroy.UserRepository;
import com.project.terabit.utility.PasswordHashing;
import com.project.terabit.validator.SignUpUserValidator;
import com.project.terabit.validator.UpdateUserValidator;



/**
 * The Class SignUpServiceImpl.
 */
@Service
@Transactional(readOnly=true)
public class SignUpUserServiceImpl implements SignUpUserService{
	
	/** The log. */
	Logger log=LoggerFactory.getLogger(this.getClass());

	/** The user repository. */
	@Autowired
	private UserRepository userRepository;
	
	/** The password hasher. */
	PasswordHashing passwordHasher = new PasswordHashing();
	
	
	/** The Constant userAlreadyExists. */
	private static final String SERVICEEXCEPTION_USERALREADYEXISTS = "SERVICE.invalid_signUp";
	
	/** The Constant successMessage. */
	private static final String SERVICEEXCEPTION_SUCCESSMESSAGE = "User has been added successfully";
	
	/** The Constant SERVICEEXCEPTION_UPDATIONSUCCESS. */
	private static final String SERVICEEXCEPTION_UPDATIONSUCCESS = "Sucessfully updated";
	
	/** The Constant SERVICEEXCEPTION_NOUSEREXIXTS. */
	private static final String SERVICEEXCEPTION_NOUSEREXIXTS = "UPDATESERVICE.invalid_uuid";
	

	/* (non-Javadoc)
	 * @see com.project.terabit.service.SignUpService#createUser(com.project.terabit.model.User)
	 */
	@Override
	@Transactional(readOnly = false,propagation=Propagation.REQUIRES_NEW)
	public User saveUser(User user) throws Exception {
		UserEntity userEntityToBeAdded=new UserEntity();
		try {
		SignUpUserValidator.validate(user);
		
		UserEntity userEntity = userRepository.checkUser(user.getUserEmailId(),user.getUserContactNo());
		
		if(userEntity!=null) {
			
			throw new ServiceException(SERVICEEXCEPTION_USERALREADYEXISTS);
		}
		
		
		userEntityToBeAdded.setIsuserAuthenticated(user.isUserAuthenticated());
		userEntityToBeAdded.setUserPassword(passwordHasher.hashing(user.getUserPassword()));
		userEntityToBeAdded.setUserContactNo(user.getUserContactNo());
		userEntityToBeAdded.setUserCreatedTime(LocalDateTime.now());
		userEntityToBeAdded.setUserEmailId(user.getUserEmailId());
		userEntityToBeAdded.setUserFirstName(user.getUserFirstName());
		userEntityToBeAdded.setUserIsActive(true);
		userEntityToBeAdded.setUserIsSeller(user.isUserIsSeller());
		userEntityToBeAdded.setUserLastName(user.getUserLastName());
		userEntityToBeAdded.setUserModifiedTime(LocalDateTime.now());
		userEntityToBeAdded.setUserPin(user.getUserPin());
		userEntityToBeAdded.setUserVerifed(user.isUserVerifed());
		user.setMessage(SERVICEEXCEPTION_SUCCESSMESSAGE);
		user.setUserIsActive(true);
		userRepository.save(userEntityToBeAdded);
		return user;
		} 
		catch(ServiceException exception){
			logg(exception.getMessage());
			throw exception;
		}catch(Exception exception) {
			logg("SignUp"+exception.getMessage());
			throw exception;
		}
		
		
	}

	/* (non-Javadoc)
	 * @see com.project.terabit.service.SignUpService#verifyUser(com.project.terabit.entity.UserEntity)
	 */
	@Override
	@Transactional(readOnly = false,propagation=Propagation.REQUIRES_NEW)
	public User updateUser(User user) throws Exception {
		
		
		try {
			UserEntity userEntityToBeUpdated;
			LocalDateTime currenttime=LocalDateTime.now();
			UpdateUserValidator.updateInfoValidate(user);
			userEntityToBeUpdated=userRepository.findUserByUserId(user.getUserId());
			if(userEntityToBeUpdated==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOUSEREXIXTS);
			}
			userEntityToBeUpdated.setUserAdminId(userEntityToBeUpdated.getUserAdminId());
			userEntityToBeUpdated.setIsuserAuthenticated(userEntityToBeUpdated.isIsuserAuthenticated());
			userEntityToBeUpdated.setUserCarts(userEntityToBeUpdated.getUserCarts());
			userEntityToBeUpdated.setUserContactNo(user.getUserContactNo());
			userEntityToBeUpdated.setUserCreatedTime(userEntityToBeUpdated.getUserCreatedTime());
			userEntityToBeUpdated.setUserEmailId(user.getUserEmailId());
			userEntityToBeUpdated.setUserFeedbacks(userEntityToBeUpdated.getUserFeedbacks());
			userEntityToBeUpdated.setUserFirstName(user.getUserFirstName());
			userEntityToBeUpdated.setUserImage(userEntityToBeUpdated.getUserImage());
			userEntityToBeUpdated.setUserIsActive(userEntityToBeUpdated.isUserIsActive());
			userEntityToBeUpdated.setUserIsSeller(userEntityToBeUpdated.isUserIsSeller());
			userEntityToBeUpdated.setUserLastName(user.getUserLastName());
			userEntityToBeUpdated.setUserModifiedTime(currenttime);
			userEntityToBeUpdated.setUserNotificationIds(userEntityToBeUpdated.getUserNotificationIds());
			userEntityToBeUpdated.setUserPin(userEntityToBeUpdated.getUserPin());
			userEntityToBeUpdated.setUserSellerId(userEntityToBeUpdated.getUserSellerId());
			userEntityToBeUpdated.setUserVerifed(userEntityToBeUpdated.isUserVerifed());
			userEntityToBeUpdated.setUserViewedProperties(userEntityToBeUpdated.getUserViewedProperties());
			userEntityToBeUpdated.setUserPassword(userEntityToBeUpdated.getUserPassword());
			user.setMessage(SERVICEEXCEPTION_UPDATIONSUCCESS);
			userRepository.save(userEntityToBeUpdated);
			return user;
		}catch(ServiceException exception) {
			logg("Update:"+exception.getMessage());
			throw exception;
		}catch(Exception exception) {
			logg(exception.getMessage());
			throw exception;
		}
		
		
	}
	
	/**
	 * Logg.
	 *
	 * @param message the message
	 */
	private void logg(String message) {
		log.error(message);
	}


	
	
}
