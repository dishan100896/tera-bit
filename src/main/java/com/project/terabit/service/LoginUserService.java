package com.project.terabit.service;


import org.hibernate.service.spi.ServiceException;
import org.springframework.stereotype.Component;

import com.project.terabit.model.User;

/**
 * The Interface LoginService.
 */
@Component
public interface LoginUserService {
	
	/**
	 * login.
	 *
	 * @param userModel the user model
	 * @return the user
	 * @throws ServiceException the service exception
	 */
	public User login(User userModel) throws ServiceException;

}
