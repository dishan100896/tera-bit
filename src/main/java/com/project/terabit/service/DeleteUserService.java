package com.project.terabit.service;

import com.project.terabit.model.User;

// TODO: Auto-generated Javadoc
/**
 * The Interface DeleteService.
 */
public interface DeleteUserService {

	
	/**
	 * Delete user.
	 *
	 * @param user the user
	 * @return the user
	 * @throws Exception the exception
	 */
	public User deleteUser(User user) throws Exception;
}
