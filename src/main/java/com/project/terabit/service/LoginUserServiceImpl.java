package com.project.terabit.service;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.project.terabit.entity.CartEntity;
import com.project.terabit.entity.FeedbackEntity;
import com.project.terabit.entity.ImageEntity;
import com.project.terabit.entity.NotificationEntity;
import com.project.terabit.entity.PropertyEntity;
import com.project.terabit.entity.UserEntity;
import com.project.terabit.entity.ViewedPropertyEntity;
import com.project.terabit.model.Admin;
import com.project.terabit.model.Cart;
import com.project.terabit.model.Feedback;
import com.project.terabit.model.Image;
import com.project.terabit.model.Notification;
import com.project.terabit.model.Property;
import com.project.terabit.model.Seller;
import com.project.terabit.model.User;
import com.project.terabit.model.ViewedProperty;
import com.project.terabit.repositroy.UserRepository;
import com.project.terabit.utility.PasswordHashing;
import com.project.terabit.utility.RandomStringGenerator;





/**
 * The Class LoginService.
 */
@Service
public class LoginUserServiceImpl implements LoginUserService {
	
	/** The log. */
	Logger log=LoggerFactory.getLogger(this.getClass());
	
	/** The userrepository. */
	@Autowired
	UserRepository userrepository;
	
	
	/** The Constant ServiceExceptionNoUser. */
	private static final String SERVICEEXCEPTION_NOUSER="SERVICE.No_User_Exists";
	
	/** The Constant ServiceExceptionUsernamePasswordMissMatch. */
	private static final String SERVICEEXCEPTION_USERNAMEMISMATCH="SERVICE.Username_Password_MissMatch";
	
	/** The Constant SERVICEEXCEPTION_USERNOTACTIVE. */
	private static final String SERVICEEXCEPTION_USERNOTACTIVE="SERVICE.User_Not_Active";
	
	/** The Constant SERVICEEXCEPTION_LOGINSUCCESS. */
	private static final String SERVICEEXCEPTION_LOGINSUCCESS="SERVICE.User_Successfully_loggedin";
	
	/**
	 * Login.
	 *
	 * @param userModel the user model
	 * @return the user
	 * @throws ServiceException the service exception
	 */
	@Transactional(readOnly=false)
	public User login (User userModel) throws ServiceException {
		User user = new User();
	
	try {
		PasswordHashing passwordHasher = new PasswordHashing();
		
		RandomStringGenerator saltstringgenerator=new RandomStringGenerator();
		
		UserEntity receivedUserEntity=null;
		
		if(userModel.getUserEmailId()!=null){
			receivedUserEntity=this.getUserByEmailId(userModel.getUserEmailId());
		}
		
		else {
			receivedUserEntity=this.getUserByContactNumber(userModel.getUserContactNo());
		}
		
		if(receivedUserEntity==null) {
			throw new ServiceException(SERVICEEXCEPTION_NOUSER);
		}
		
		String hashedInputpassword=passwordHasher.hashing(userModel.getUserPassword());
		if(!receivedUserEntity.isUserIsActive()) {
			throw new ServiceException (SERVICEEXCEPTION_USERNOTACTIVE);
		}
		if(!checkPassword(hashedInputpassword,receivedUserEntity.getUserPassword())){
			throw new ServiceException(SERVICEEXCEPTION_USERNAMEMISMATCH);
		}
				user.setSaltstring(saltstringgenerator.randomString());
				user.setUserAuthenticated(receivedUserEntity.isIsuserAuthenticated());
				user.setUserContactNo(receivedUserEntity.getUserContactNo());
				user.setUserCreatedTime(receivedUserEntity.getUserCreatedTime());
				user.setUserEmailId(receivedUserEntity.getUserEmailId());
				user.setUserFirstName(receivedUserEntity.getUserFirstName());
				user.setUserId(receivedUserEntity.getUserId());
				user.setUserIsActive(receivedUserEntity.isUserIsActive());
				user.setUserIsSeller(receivedUserEntity.isUserIsSeller());
				user.setUserLastName(receivedUserEntity.getUserLastName());
				user.setUserModifiedTime(receivedUserEntity.getUserModifiedTime());
				user.setUserPin(receivedUserEntity.getUserPin());
				user.setUserVerifed(receivedUserEntity.isUserVerifed());
				//admin for user
				if(receivedUserEntity.getUserAdminId()!=null) {
					user.setUserAdminId(this.adminForUser(receivedUserEntity));
				}
				//image to user
				if(receivedUserEntity.getUserImage()!=null) {
					user.setUserImage(this.imageForUser(receivedUserEntity.getUserImage()));
				}
				//cart to user
				if(!receivedUserEntity.getUserCarts().isEmpty()) {	
					user.setUserCarts(this.cartForUser(receivedUserEntity.getUserCarts()));
				}
				//seller to user
				if(receivedUserEntity.getUserSellerId()!=null) {
					//setting seller to user
					user.setUserSellerId(this.sellerForUser(receivedUserEntity));
				}
				if(!receivedUserEntity.getUserViewedProperties().isEmpty()) {
				// setting viewed property
				user.setUserViewedProperties(this.viewedproperty(receivedUserEntity.getUserViewedProperties()));
				}
				
				//setting Feedback of user from user entity
				if(!receivedUserEntity.getUserFeedbacks().isEmpty()) {
					user.setUserFeedbacks(this.feedback(receivedUserEntity.getUserFeedbacks()));
				}
				user.setMessage(SERVICEEXCEPTION_LOGINSUCCESS);	
				this.setSaltString(receivedUserEntity, user.getSaltstring());
		}
	catch(ServiceException exception) {
		
		logg(exception.getMessage());
		throw exception;
	}catch(Exception exception) {
		logg("Login "+exception.getMessage());
		throw exception;
	}
	return user;
	
	}
	
	
	
	/**
	 * Admin for user.
	 *
	 * @param receivedUserEntity the received user entity
	 * @return the admin
	 */
	public Admin adminForUser(UserEntity receivedUserEntity){
		try {
		Admin admin=new Admin();
		admin.setAdminCreatedTime(receivedUserEntity.getUserAdminId().getAdminCreatedTime());
		admin.setAdminId(receivedUserEntity.getUserAdminId().getAdminId());
		admin.setAdminIsActive(receivedUserEntity.getUserAdminId().isAdminIsActive());
		admin.setAdminRightsBy(receivedUserEntity.getUserAdminId().getAdminRightsBy());
		admin.setAdminSellerCount(receivedUserEntity.getUserAdminId().getAdminSellerCount());
		admin.setAdminSellerId(receivedUserEntity.getUserAdminId().getAdminSellerId());
		if(!receivedUserEntity.getUserAdminId().getAdminFeedbackId().isEmpty()) {
			admin.setAdminFeedbackIds(this.feedback(receivedUserEntity.getUserAdminId().getAdminFeedbackId()));
		}
		if(!receivedUserEntity.getUserAdminId().getAdminNotificationId().isEmpty()) {
			admin.setAdminNotificationIds(this.notificationlist(receivedUserEntity.getUserAdminId().getAdminNotificationId()));
		}
		return admin;
		}catch(Exception exception) {
			logg("adminForUser "+exception.getMessage());
			throw exception;
		}
	}
	
	
	/**
	 * Feedback.
	 *
	 * @param feedbackentitylist the feedbackentitylist
	 * @return the list
	 */
	public List<Feedback> feedback(List<FeedbackEntity> feedbackentitylist){
		try {
		List<Feedback> feedbacklist=new ArrayList<>();
		for (FeedbackEntity feedback1 : feedbackentitylist) {
			Feedback feedbackToAddToAdmin= new Feedback();
			feedbackToAddToAdmin.setFeedbackCreatedBy(feedback1.getFeedbackCreatedBy());
			feedbackToAddToAdmin.setFeedbackDescription(feedback1.getFeedbackDescription());
			feedbackToAddToAdmin.setFeedbackGivenBy(feedback1.getFeedbackGivenBy());
			feedbackToAddToAdmin.setFeedbackId(feedback1.getFeedbackId());
			feedbackToAddToAdmin.setFeedbackRating(feedback1.getFeedbackRating());
			feedbacklist.add(feedbackToAddToAdmin);
		}
		return feedbacklist;
		}catch(Exception exception) {
			logg("feedback "+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Viewedproperty.
	 *
	 * @param viewedPropertyList the viewed property list
	 * @return the list
	 */
	public List<ViewedProperty> viewedproperty (List<ViewedPropertyEntity> viewedPropertyList){
		try {
		List<ViewedProperty> viewedPropertyOfSeller=new ArrayList<>();
		for (ViewedPropertyEntity viewedproperty : viewedPropertyList) {
			ViewedProperty viewproperty=new ViewedProperty();
			viewproperty.setViewedPropertyId(viewedproperty.getViewedPropertyId());
			viewproperty.setViewedPropertyPropertyId(viewedproperty.getViewedPropertyPropertyId());
			viewproperty.setViewedSellerId(viewedproperty.getViewedSellerId());
			viewproperty.setViewedTime(viewedproperty.getViewedTime());
			viewproperty.setViewedUserId(viewedproperty.getViewedUserId());
			viewedPropertyOfSeller.add(viewproperty);
		}
		return viewedPropertyOfSeller;
		}catch(Exception exception) {
			logg("viewedproperty "+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Image for user.
	 *
	 * @param userImageEntity the user image entity
	 * @return the image
	 */
	public Image imageForUser(ImageEntity userImageEntity) {
		try {
		Image image= new Image();
		image.setImageCreatedTime(userImageEntity.getImageCreatedTime());
		image.setImageDescription(userImageEntity.getImageDescription());
		image.setImageId(userImageEntity.getImageId());
		image.setImageIsActive(userImageEntity.isImageIsActive());
		image.setImagePath(userImageEntity.getImagePath());
		return image;
		}catch(Exception exception) {
			logg("imageForUser "+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Cart for user.
	 *
	 * @param cartentitylist the cartentitylist
	 * @return the list
	 */
	public List<Cart> cartForUser(List<CartEntity> cartentitylist){
		try {
		List<CartEntity> cartEntityList=cartentitylist;
		List<Cart> cartListToBeSet= new ArrayList<>();
		for (CartEntity cartEntity : cartEntityList) {
			Cart cart = new Cart();
			cart.setCartCreatedTime(cartEntity.getCartCreatedTime());
			cart.setCartId(cartEntity.getCartId());
			cart.setCartIsActive(cartEntity.isCartIsActive());
			cart.setCartPropertyId(cartEntity.getCartPropertyId());
			cart.setCartUserId(cartEntity.getCartUserId());
			cartListToBeSet.add(cart);
		}
		return cartListToBeSet;
		}catch(Exception exception) {
			logg("cartForUser "+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Notificationlist.
	 *
	 * @param notificationentitylist the notificationentitylist
	 * @return the list
	 */
	public List<Notification> notificationlist(List<NotificationEntity> notificationentitylist){
		try {
		List<Notification> notificationListToBeSetToSeller=new ArrayList<>();
		for (NotificationEntity notificationEntity : notificationentitylist) {
			Notification notification = new Notification();
			notification.setNotificationContent(notificationEntity.getNotificationContent());
			notification.setNotificationId(notificationEntity.getNotificationId());
			notification.setNotificationIsActive(notificationEntity.isNotificationIsActive());
			notification.setNotificationNotifiedTime(notificationEntity.getNotificationNotifiedTime());
			notification.setNotificationTo(notificationEntity.getNotificationTo());
			notification.setNotificationViewedTime(notificationEntity.getNotificationViewedTime());
			notificationListToBeSetToSeller.add(notification);
		}
		return notificationListToBeSetToSeller;
		}catch(Exception exception) {
			logg("notificationlist "+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Seller for user.
	 *
	 * @param receivedUserEntity the received user entity
	 * @return the seller
	 */
	public Seller sellerForUser(UserEntity receivedUserEntity) {
		try {
		Seller sellerToBeSetToUser = new Seller();
		sellerToBeSetToUser.setSellerCompanyName(receivedUserEntity.getUserSellerId().getSellerCompanyName());
		sellerToBeSetToUser.setSellerCreatedTime(receivedUserEntity.getUserSellerId().getSellerCreatedTime());
		sellerToBeSetToUser.setSellerId(receivedUserEntity.getUserSellerId().getSellerId());
		sellerToBeSetToUser.setSellerIsActive(receivedUserEntity.getUserSellerId().isSellerIsActive());
		sellerToBeSetToUser.setSellerPrivateContact(receivedUserEntity.getUserSellerId().getSellerPrivateContact());
		sellerToBeSetToUser.setSellerPropertyCount(receivedUserEntity.getUserSellerId().getSellerPropertyCount());
		sellerToBeSetToUser.setSellerRightBy(receivedUserEntity.getUserSellerId().getSellerRightBy());
		if(receivedUserEntity.getUserSellerId().getSellerCompanyLogoId()!=null) {
			sellerToBeSetToUser.setSellerCompanyLogoId(this.imageForUser(receivedUserEntity.getUserSellerId().getSellerCompanyLogoId()));
		}
		if((!receivedUserEntity.getUserSellerId().getSellerFeedbackId().isEmpty())){
			sellerToBeSetToUser.setSellerFeedbackIds(this.feedback(receivedUserEntity.getUserSellerId().getSellerFeedbackId()));
		}
		if(!receivedUserEntity.getUserSellerId().getSellerNotificationId().isEmpty()) {
			sellerToBeSetToUser.setSellerNotificationIds(this.notificationlist(receivedUserEntity.getUserSellerId().getSellerNotificationId()));
		}
		if(!receivedUserEntity.getUserSellerId().getSellerPropertyId().isEmpty()) {
			sellerToBeSetToUser.setSellerPropertyIds(this.propertysetting(receivedUserEntity.getUserSellerId().getSellerPropertyId()));
		}
		return sellerToBeSetToUser;
		}
		catch(Exception exception) {
			logg("sellerForUser "+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Propertysetting.
	 *
	 * @param propertyEntityList the property entity list
	 * @return the list
	 */
	public List<Property> propertysetting(List<PropertyEntity> propertyEntityList){
		try {
		List<Property> propertyListToSetToSeller=new ArrayList<>();
		for (PropertyEntity property : propertyEntityList) {
			Property propertyToSetToSeller = new Property();
			propertyToSetToSeller.setPropertyCent(property.getPropertyCent());
			propertyToSetToSeller.setPropertyCity(property.getPropertyCity());
			propertyToSetToSeller.setPropertyCountry(property.getPropertyCountry());
			propertyToSetToSeller.setPropertyCreatedTime(property.getPropertyCreatedTime());
			propertyToSetToSeller.setPropertyDescription(property.getPropertyDescription());
			propertyToSetToSeller.setPropertyEsteematedAmount(property.getPropertyEsteematedAmount());
			propertyToSetToSeller.setPropertyId(property.getPropertyId());
			propertyToSetToSeller.setPropertyIsActive(property.isPropertyIsActive());
			propertyToSetToSeller.setPropertyLandmark(property.getPropertyLandmark());
			propertyToSetToSeller.setPropertyLatitude(property.getPropertyLongitude());
			propertyToSetToSeller.setPropertyModifiedTime(property.getPropertyModifiedTime());
			propertyToSetToSeller.setPropertyOwnedBy(property.getPropertyOwnedBy());
			propertyToSetToSeller.setPropertyState(property.getPropertyState());
			propertyToSetToSeller.setPropertyType(property.getPropertyType());
			propertyToSetToSeller.setPropertyViewedCount(property.getPropertyViewedCount());
			propertyToSetToSeller.setPropertyLongitude(property.getPropertyLongitude());
			if(!property.getPropertyFeedbackId().isEmpty()) {
				propertyToSetToSeller.setPropertyFeedbackIds(this.feedback(property.getPropertyFeedbackId()));
			}
			if(!property.getPropertyImageId().isEmpty()) {
				List<Image> imageOfPropertyListToSetToSeller=new ArrayList<>();
				for (ImageEntity image : property.getPropertyImageId() ) {
					Image image1=this.imageForUser(image);
					imageOfPropertyListToSetToSeller.add(image1);
				}
				propertyToSetToSeller.setPropertyImageIds(imageOfPropertyListToSetToSeller);
			}
			if(!property.getPropertyViewedId().isEmpty()) {
				propertyToSetToSeller.setPropertyViewedIds(this.viewedproperty(property.getPropertyViewedId()));
			}
			propertyListToSetToSeller.add(propertyToSetToSeller);
			
		}
		return propertyListToSetToSeller;
		}catch(Exception exception) {
			logg("propertySetting "+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Gets the user by email id.
	 *
	 * @param emailId the email id
	 * @return the user entity
	 */
	public UserEntity getUserByEmailId(String emailId) {
		try {
		
		return userrepository.findUserByEmailID(emailId);
		}catch(Exception exception) {
			logg("getUserByEmailId "+exception.getMessage());
			throw exception;
		}
		
		
	}
	
	/**
	 * Check password.
	 *
	 * @param hashedInputpassword the hashed inputpassword
	 * @param dbPassword the db password
	 * @return true, if successful
	 */
	public boolean checkPassword(String hashedInputpassword,String dbPassword) {
		try {
		boolean flag=false;
		if(dbPassword.equals(hashedInputpassword)) {
			
			flag=true;
		}
		
		return flag;
		}catch(Exception exception) {
			logg("checkPassword "+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Gets the user by contact number.
	 *
	 * @param contactNumber the contact number
	 * @return the user entity
	 */
	public UserEntity getUserByContactNumber(BigInteger contactNumber) {
		try {
		
		return userrepository.findUserByContactNumber(contactNumber);
		}catch(Exception exception) {
			logg("getUserByContactNumber "+exception.getMessage());
			throw exception;
		}
		
	}
	
	/**
	 * Sets the salt string.
	 *
	 * @param receivedUserEntity the received user entity
	 * @param saltstring the saltstring
	 */
	@Transactional(readOnly=false)
	public void setSaltString(UserEntity receivedUserEntity, String saltstring) {
		try {
		receivedUserEntity.setSaltString(saltstring);
		userrepository.save(receivedUserEntity);
	}catch(Exception exception) {
		logg("setSaltString "+exception.getMessage());
		throw exception;
	}
	}
	
	
	
	/**
	 * Logg.
	 *
	 * @param message the message
	 */
	private void logg(String message) {
		log.error(message);
	}
	
}
