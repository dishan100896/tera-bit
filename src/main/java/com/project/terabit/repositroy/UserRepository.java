package com.project.terabit.repositroy;

import java.math.BigInteger;
import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import com.project.terabit.entity.UserEntity;



/**
 * The Interface UserRepository.
 */
@Repository
public interface UserRepository extends CrudRepository<UserEntity, UUID>  {
	
	/**
	 * Find user by email ID.
	 *
	 * @param emailId the email id
	 * @return the user entity
	 */
	@Query(value="SELECT * FROM users u  WHERE u.user_email_id LIKE :emailId", nativeQuery=true)
	UserEntity findUserByEmailID(@Param(value="emailId") String emailId);
	
	/**
	 * Find user by contact number.
	 *
	 * @param contactNumber the contact number
	 * @return the user entity
	 */
	@Query(value="SELECT * FROM users u  WHERE u.user_contact_no = :contactNumber", nativeQuery=true)
	UserEntity findUserByContactNumber(@Param(value="contactNumber") BigInteger contactNumber);
	
	/**
	 * Check user.
	 *
	 * @param mailId the mail id
	 * @param contactNumber the contact number
	 * @return the user entity
	 */
	@Query(value = "SELECT * FROM users u WHERE u.user_email_id like :mailId OR u.user_contact_no = :contactNumber", nativeQuery = true)
	public UserEntity checkUser(@RequestParam String mailId, @RequestParam BigInteger contactNumber);
	
	/**
	 * Find user by user id.
	 *
	 * @param userId the user id
	 * @return the user entity
	 */
	@Query(value ="SELECT * FROM users u WHERE u.user_id = :userId",nativeQuery=true)
	public UserEntity findUserByUserId(@Param(value="userId") UUID userId);
 
}
