package com.project.terabit.validator;

public class DeleteUserValidator {
	
	
	private DeleteUserValidator() {
	    throw new IllegalStateException("Utility class");
	  }

	
	private static final String USERIDEXCEPTION = "DELETEVALIDATOR.invalid_user_id";
	
	
	public static void validate(String userId) throws Exception {
		if(!validateUserId(userId))  throw new Exception(USERIDEXCEPTION);
	}

	public static Boolean validateUserId(String userId) {
		Boolean flag = false;
		if(userId.matches("[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}")) {
			flag = true;
		}return flag;
	}

}
