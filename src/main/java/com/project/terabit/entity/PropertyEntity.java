package com.project.terabit.entity;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;




/**
 * The Class PropertyEntity.
 */
@Entity
@Table(name="Property")
public class PropertyEntity {
	
	/** The property id. */
	@Id
	@GeneratedValue
	@NotNull
	@Column(name="property_id")
	private BigInteger propertyId;
	
	/** The property type. */
	@NotNull
	@Column(name="property_type")
	private String propertyType;
	
	/** The property owned by. */
	@NotNull
	@Column(name="property_owned_by")
	private int propertyOwnedBy;
	
	/** The property landmark. */
	@NotNull
	@Column(name="property_landmark")
	private String propertyLandmark;
	
	/** The property city. */
	@NotNull
	@Column(name="property_city")
	private String propertyCity;
	
	/** The property state. */
	@NotNull
	@Column(name="property_state")
	private String propertyState;
	
	/** The property country. */
	@NotNull
	@Column(name="property_country")
	private String propertyCountry;
	
	
	/** The property latitude. */
	@Column(name="property_latitude")
	private String propertyLatitude;
	
	/** The property longitude. */
	@Column(name="property_longitude")
	private String propertyLongitude;
	
	/** The property description. */
	@NotNull
	@Column(name="property_description")
	private String propertyDescription;
	
	/** The property cent. */
	@NotNull
	@Column(name="property_cent")
	private BigInteger propertyCent;
	
	/** The property is active. */
	@NotNull
	@Column(name="property_is_active")
	private boolean propertyIsActive=false;
	
	/** The property created time. */
	@NotNull
	@Column(name="property_created_time")
	private LocalDateTime propertyCreatedTime;
	
	/** The property modified time. */
	@NotNull
	@Column(name="property_modified_time")
	private LocalDateTime propertyModifiedTime;
	
	/** The property esteemated amount. */
	@NotNull
	@Column(name="property_esteemated_amount")
	private String propertyEsteematedAmount;
	
	/** The property feedback id. */
	@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	private List<FeedbackEntity> propertyFeedbackIds=new ArrayList<>();
	
	/** The property viewed count. */
	@NotNull
	@Column(name="property_viewed_count")
	private BigInteger propertyViewedCount;
	
	/** The property viewed id. */
	@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	@Column(name="property_viewed_id")
	private List<ViewedPropertyEntity> propertyViewedIds = new ArrayList<>();
	
	/** The property image id. */
	@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	private List<ImageEntity> propertyImageIds = new ArrayList<>();

	/**
	 * Gets the property id.
	 *
	 * @return the property id
	 */
	public BigInteger getPropertyId() {
		return propertyId;
	}

	/**
	 * Sets the property id.
	 *
	 * @param propertyId the new property id
	 */
	public void setPropertyId(BigInteger propertyId) {
		this.propertyId = propertyId;
	}

	/**
	 * Gets the property type.
	 *
	 * @return the property type
	 */
	public String getPropertyType() {
		return propertyType;
	}

	/**
	 * Sets the property type.
	 *
	 * @param propertyType the new property type
	 */
	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}

	/**
	 * Gets the property owned by.
	 *
	 * @return the property owned by
	 */
	public int getPropertyOwnedBy() {
		return propertyOwnedBy;
	}

	/**
	 * Sets the property owned by.
	 *
	 * @param propertyOwnedBy the new property owned by
	 */
	public void setPropertyOwnedBy(int propertyOwnedBy) {
		this.propertyOwnedBy = propertyOwnedBy;
	}

	/**
	 * Gets the property landmark.
	 *
	 * @return the property landmark
	 */
	public String getPropertyLandmark() {
		return propertyLandmark;
	}

	/**
	 * Sets the property landmark.
	 *
	 * @param propertyLandmark the new property landmark
	 */
	public void setPropertyLandmark(String propertyLandmark) {
		this.propertyLandmark = propertyLandmark;
	}

	/**
	 * Gets the property city.
	 *
	 * @return the property city
	 */
	public String getPropertyCity() {
		return propertyCity;
	}

	/**
	 * Sets the property city.
	 *
	 * @param propertyCity the new property city
	 */
	public void setPropertyCity(String propertyCity) {
		this.propertyCity = propertyCity;
	}

	/**
	 * Gets the property state.
	 *
	 * @return the property state
	 */
	public String getPropertyState() {
		return propertyState;
	}

	/**
	 * Sets the property state.
	 *
	 * @param propertyState the new property state
	 */
	public void setPropertyState(String propertyState) {
		this.propertyState = propertyState;
	}

	/**
	 * Gets the property country.
	 *
	 * @return the property country
	 */
	public String getPropertyCountry() {
		return propertyCountry;
	}

	/**
	 * Sets the property country.
	 *
	 * @param propertyCountry the new property country
	 */
	public void setPropertyCountry(String propertyCountry) {
		this.propertyCountry = propertyCountry;
	}

	/**
	 * Gets the property latitude.
	 *
	 * @return the property latitude
	 */
	public String getPropertyLatitude() {
		return propertyLatitude;
	}

	/**
	 * Sets the property latitude.
	 *
	 * @param propertyLatitude the new property latitude
	 */
	public void setPropertyLatitude(String propertyLatitude) {
		this.propertyLatitude = propertyLatitude;
	}

	/**
	 * Gets the property longitude.
	 *
	 * @return the property longitude
	 */
	public String getPropertyLongitude() {
		return propertyLongitude;
	}

	/**
	 * Sets the property longitude.
	 *
	 * @param propertyLongitude the new property longitude
	 */
	public void setPropertyLongitude(String propertyLongitude) {
		this.propertyLongitude = propertyLongitude;
	}

	/**
	 * Gets the property description.
	 *
	 * @return the property description
	 */
	public String getPropertyDescription() {
		return propertyDescription;
	}

	/**
	 * Sets the property description.
	 *
	 * @param propertyDescription the new property description
	 */
	public void setPropertyDescription(String propertyDescription) {
		this.propertyDescription = propertyDescription;
	}

	/**
	 * Gets the property cent.
	 *
	 * @return the property cent
	 */
	public BigInteger getPropertyCent() {
		return propertyCent;
	}

	/**
	 * Sets the property cent.
	 *
	 * @param propertyCent the new property cent
	 */
	public void setPropertyCent(BigInteger propertyCent) {
		this.propertyCent = propertyCent;
	}

	/**
	 * Checks if is property is active.
	 *
	 * @return true, if is property is active
	 */
	public boolean isPropertyIsActive() {
		return propertyIsActive;
	}

	/**
	 * Sets the property is active.
	 *
	 * @param propertyIsActive the new property is active
	 */
	public void setPropertyIsActive(boolean propertyIsActive) {
		this.propertyIsActive = propertyIsActive;
	}

	/**
	 * Gets the property created time.
	 *
	 * @return the property created time
	 */
	public LocalDateTime getPropertyCreatedTime() {
		return propertyCreatedTime;
	}

	/**
	 * Sets the property created time.
	 *
	 * @param propertyCreatedTime the new property created time
	 */
	public void setPropertyCreatedTime(LocalDateTime propertyCreatedTime) {
		this.propertyCreatedTime = propertyCreatedTime;
	}

	/**
	 * Gets the property modified time.
	 *
	 * @return the property modified time
	 */
	public LocalDateTime getPropertyModifiedTime() {
		return propertyModifiedTime;
	}

	/**
	 * Sets the property modified time.
	 *
	 * @param propertyModifiedTime the new property modified time
	 */
	public void setPropertyModifiedTime(LocalDateTime propertyModifiedTime) {
		this.propertyModifiedTime = propertyModifiedTime;
	}

	/**
	 * Gets the property esteemated amount.
	 *
	 * @return the property esteemated amount
	 */
	public String getPropertyEsteematedAmount() {
		return propertyEsteematedAmount;
	}

	/**
	 * Sets the property esteemated amount.
	 *
	 * @param propertyEsteematedAmount the new property esteemated amount
	 */
	public void setPropertyEsteematedAmount(String propertyEsteematedAmount) {
		this.propertyEsteematedAmount = propertyEsteematedAmount;
	}

	/**
	 * Gets the property feedback id.
	 *
	 * @return the property feedback id
	 */
	public List<FeedbackEntity> getPropertyFeedbackId() {
		return propertyFeedbackIds;
	}

	/**
	 * Sets the property feedback id.
	 *
	 * @param propertyFeedbackIds the new property feedback id
	 */
	public void setPropertyFeedbackId(List<FeedbackEntity> propertyFeedbackIds) {
		this.propertyFeedbackIds = propertyFeedbackIds;
	}

	/**
	 * Gets the property viewed count.
	 *
	 * @return the property viewed count
	 */
	public BigInteger getPropertyViewedCount() {
		return propertyViewedCount;
	}

	/**
	 * Sets the property viewed count.
	 *
	 * @param propertyViewedCount the new property viewed count
	 */
	public void setPropertyViewedCount(BigInteger propertyViewedCount) {
		this.propertyViewedCount = propertyViewedCount;
	}

	/**
	 * Gets the property viewed id.
	 *
	 * @return the property viewed id
	 */
	public List<ViewedPropertyEntity> getPropertyViewedId() {
		return propertyViewedIds;
	}

	/**
	 * Sets the property viewed id.
	 *
	 * @param propertyViewedIds the new property viewed id
	 */
	public void setPropertyViewedId(List<ViewedPropertyEntity> propertyViewedIds) {
		this.propertyViewedIds = propertyViewedIds;
	}

	/**
	 * Gets the property image id.
	 *
	 * @return the property image id
	 */
	public List<ImageEntity> getPropertyImageId() {
		return propertyImageIds;
	}

	/**
	 * Sets the property image id.
	 *
	 * @param propertyImageIds the new property image id
	 */
	public void setPropertyImageId(List<ImageEntity> propertyImageIds) {
		this.propertyImageIds = propertyImageIds;
	}
	
	
	
}
