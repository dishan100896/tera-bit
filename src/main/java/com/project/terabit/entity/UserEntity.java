package com.project.terabit.entity;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

// TODO: Auto-generated Javadoc
/**
 * The Class UserEntity.
 */
@Entity
@Table(name="users")
public class UserEntity {
	
	/** The user id. */
	@Id
	@GeneratedValue
	@Column(name="user_id")
	@NotNull
	private UUID userId;
	
	/** The user first name. */
	@Column(name="user_first_name")
	@NotNull
	private String userFirstName;
	
	/** The user last name. */
	@Column(name="user_last_name")
	@NotNull
	private String userLastName;
	
	/** The user password. */
	@Column(name="user_password")
	@NotNull
	private String userPassword;
	
	/** The user email id. */
	@Column(name="user_email_id")
	@NotNull
	private String userEmailId;
	
	/** The user contact no. */
	@Column(name="user_contact_no")
	@NotNull
	private BigInteger userContactNo;
	
	/** The user verification. */
	@Column(name="user_is_verified")
	@NotNull
	private boolean userVerifed=false;
	
	/** The user authentification. */
	@NotNull
	@Column(name="user_is_authenticated")
	private boolean isuserAuthenticated=false;
	
	/** The salt string. */
	@NotNull
	@Column(name="user_salt_string")
	private String saltString="aaaaaaaaaaaa";


	/** The user pin. */
	@Column(name="user_pin")
	private int userPin;
	
	/** The user is seller. */
	@Column(name="user_is_seller")
	@NotNull
	private boolean userIsSeller=false;
	
	/** The user is active. */
	@Column(name="user_is_active")
	@NotNull
	private boolean userIsActive=false;
	
	/** The user created time. */
	@Column(name="user_created_time")
	@NotNull
	private LocalDateTime userCreatedTime;
	
	/** The user modified time. */
	@Column(name="user_modified_time")
	@NotNull
	private LocalDateTime userModifiedTime;

	/** The user cart. */
	@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	private List<CartEntity> userCarts = new ArrayList<>();
	
	/** The user seller id. */
	@OneToOne
	@JoinColumn(name="seller_id")
	private SellerEntity userSellerId;
	
	/** The user admin id. */
	@OneToOne
	@JoinColumn(name="admin_id")
	private AdminEntity userAdminId;
	
	/** The user feedback. */
	@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	private List<FeedbackEntity> userFeedbacks = new ArrayList<>();
	
	/** The user notification id. */
	@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	private List<NotificationEntity> userNotificationIds = new ArrayList<>(); 
	
	/** The user viewed property. */
	@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	private List<ViewedPropertyEntity> userViewedProperties = new ArrayList<>();
	
	/** The user image. */
	@OneToOne
	@JoinColumn(name="image_id")
	private ImageEntity userImage;

	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public UUID getUserId() {
		return userId;
	}

	/**
	 * Sets the user id.
	 *
	 * @param userId the new user id
	 */
	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	/**
	 * Gets the user first name.
	 *
	 * @return the user first name
	 */
	public String getUserFirstName() {
		return userFirstName;
	}

	/**
	 * Sets the user first name.
	 *
	 * @param userFirstName the new user first name
	 */
	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}

	/**
	 * Gets the user last name.
	 *
	 * @return the user last name
	 */
	public String getUserLastName() {
		return userLastName;
	}

	/**
	 * Sets the user last name.
	 *
	 * @param userLastName the new user last name
	 */
	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}

	/**
	 * Gets the user password.
	 *
	 * @return the user password
	 */
	public String getUserPassword() {
		return userPassword;
	}

	/**
	 * Sets the user password.
	 *
	 * @param userPassword the new user password
	 */
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	/**
	 * Gets the user email id.
	 *
	 * @return the user email id
	 */
	public String getUserEmailId() {
		return userEmailId;
	}

	/**
	 * Sets the user email id.
	 *
	 * @param userEmailId the new user email id
	 */
	public void setUserEmailId(String userEmailId) {
		this.userEmailId = userEmailId;
	}

	/**
	 * Gets the user contact no.
	 *
	 * @return the user contact no
	 */
	public BigInteger getUserContactNo() {
		return userContactNo;
	}

	/**
	 * Sets the user contact no.
	 *
	 * @param userContactNo the new user contact no
	 */
	public void setUserContactNo(BigInteger userContactNo) {
		this.userContactNo = userContactNo;
	}

	/**
	 * Checks if is user verifed.
	 *
	 * @return true, if is user verifed
	 */
	public boolean isUserVerifed() {
		return userVerifed;
	}

	/**
	 * Sets the user verifed.
	 *
	 * @param userVerifed the new user verifed
	 */
	public void setUserVerifed(boolean userVerifed) {
		this.userVerifed = userVerifed;
	}

	/**
	 * Gets the user authenticated.
	 *
	 * @return the user authenticated
	 */
	

	public boolean isIsuserAuthenticated() {
		return isuserAuthenticated;
	}

	/**
	 * Sets the isuser authenticated.
	 *
	 * @param isuserAuthenticated the new isuser authenticated
	 */
	public void setIsuserAuthenticated(boolean isuserAuthenticated) {
		this.isuserAuthenticated = isuserAuthenticated;
	}
	
	
	/**
	 * Gets the salt string.
	 *
	 * @return the salt string
	 */
	public String getSaltString() {
		return saltString;
	}


	/**
	 * Sets the salt string.
	 *
	 * @param saltString the new salt string
	 */
	public void setSaltString(String saltString) {
		this.saltString = saltString;
	}

	/**
	 * Gets the user pin.
	 *
	 * @return the user pin
	 */
	public int getUserPin() {
		return userPin;
	}

	/**
	 * Sets the user pin.
	 *
	 * @param userPin the new user pin
	 */
	public void setUserPin(int userPin) {
		this.userPin = userPin;
	}

	/**
	 * Checks if is user is seller.
	 *
	 * @return true, if is user is seller
	 */
	public boolean isUserIsSeller() {
		return userIsSeller;
	}

	/**
	 * Sets the user is seller.
	 *
	 * @param userIsSeller the new user is seller
	 */
	public void setUserIsSeller(boolean userIsSeller) {
		this.userIsSeller = userIsSeller;
	}

	/**
	 * Checks if is user is active.
	 *
	 * @return true, if is user is active
	 */
	public boolean isUserIsActive() {
		return userIsActive;
	}

	/**
	 * Sets the user is active.
	 *
	 * @param userIsActive the new user is active
	 */
	public void setUserIsActive(boolean userIsActive) {
		this.userIsActive = userIsActive;
	}

	/**
	 * Gets the user created time.
	 *
	 * @return the user created time
	 */
	public LocalDateTime getUserCreatedTime() {
		return userCreatedTime;
	}

	/**
	 * Sets the user created time.
	 *
	 * @param userCreatedTime the new user created time
	 */
	public void setUserCreatedTime(LocalDateTime userCreatedTime) {
		this.userCreatedTime = userCreatedTime;
	}

	/**
	 * Gets the user modified time.
	 *
	 * @return the user modified time
	 */
	public LocalDateTime getUserModifiedTime() {
		return userModifiedTime;
	}

	/**
	 * Sets the user modified time.
	 *
	 * @param userModifiedTime the new user modified time
	 */
	public void setUserModifiedTime(LocalDateTime userModifiedTime) {
		this.userModifiedTime = userModifiedTime;
	}

	/**
	 * Gets the user cart.
	 *
	 * @return the user cart
	 */
	public List<CartEntity> getUserCarts() {
		return userCarts;
	}

	/**
	 * Sets the user cart.
	 *
	 * @param userCarts the new user carts
	 */
	public void setUserCarts(List<CartEntity> userCarts) {
		this.userCarts = userCarts;
	}

	/**
	 * Gets the user seller id.
	 *
	 * @return the user seller id
	 */
	public SellerEntity getUserSellerId() {
		return userSellerId;
	}

	/**
	 * Sets the user seller id.
	 *
	 * @param userSellerId the new user seller id
	 */
	public void setUserSellerId(SellerEntity userSellerId) {
		this.userSellerId = userSellerId;
	}

	/**
	 * Gets the user admin id.
	 *
	 * @return the user admin id
	 */
	public AdminEntity getUserAdminId() {
		return userAdminId;
	}

	/**
	 * Sets the user admin id.
	 *
	 * @param userAdminId the new user admin id
	 */
	public void setUserAdminId(AdminEntity userAdminId) {
		this.userAdminId = userAdminId;
	}

	/**
	 * Gets the user feedback.
	 *
	 * @return the user feedback
	 */
	public List<FeedbackEntity> getUserFeedbacks() {
		return userFeedbacks;
	}

	/**
	 * Sets the user feedback.
	 *
	 * @param userFeedbacks the new user feedbacks
	 */
	public void setUserFeedbacks(List<FeedbackEntity> userFeedbacks) {
		this.userFeedbacks = userFeedbacks;
	}

	/**
	 * Gets the user notification id.
	 *
	 * @return the user notification id
	 */
	public List<NotificationEntity> getUserNotificationIds() {
		return userNotificationIds;
	}

	/**
	 * Sets the user notification id.
	 *
	 * @param userNotificationIds the new user notification ids
	 */
	public void setUserNotificationIds(List<NotificationEntity> userNotificationIds) {
		this.userNotificationIds = userNotificationIds;
	}

	/**
	 * Gets the user viewed property.
	 *
	 * @return the user viewed property
	 */
	public List<ViewedPropertyEntity> getUserViewedProperties() {
		return userViewedProperties;
	}

	/**
	 * Sets the user viewed property.
	 *
	 * @param userViewedProperties the new user viewed properties
	 */
	public void setUserViewedProperties(List<ViewedPropertyEntity> userViewedProperties) {
		this.userViewedProperties = userViewedProperties;
	}

	/**
	 * Gets the user image.
	 *
	 * @return the user image
	 */
	public ImageEntity getUserImage() {
		return userImage;
	}

	/**
	 * Sets the user image.
	 *
	 * @param userImage the new user image
	 */
	public void setUserImage(ImageEntity userImage) {
		this.userImage = userImage;
	}
	
	
	
}
