package com.project.terabit.entity;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * The Class SellerEntity.
 */
@Entity
@Table(name="seller")
public class SellerEntity {
	
	/** The seller id. */
	@Id
	@GeneratedValue
	@NotNull
	@Column(name="seller_id")
	private BigInteger sellerId;
	
	/** The seller property id. */
	@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	private List<PropertyEntity> sellerPropertyIds = new ArrayList<>();
	
	/** The seller right by. */
	@Column(name="seller_right_by")
	private BigInteger sellerRightBy;
	
	/** The seller is active. */
	@NotNull
	@Column(name="seller_is_active")
	private boolean sellerIsActive=false;
	
	/** The seller notification id. */
	@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	private List<NotificationEntity> sellerNotificationIds = new ArrayList<>();
	
	/** The seller feedback id. */
	@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	private List<FeedbackEntity> sellerFeedbackIds = new ArrayList<>(); 
	
	/** The seller created time. */
	@NotNull
	@Column(name="seller_created_time")
	private LocalDateTime sellerCreatedTime;
	
	/** The seller private contact. */
	@NotNull
	@Column(name="seller_private_contact")
	private BigInteger sellerPrivateContact;
	
	/** The seller company name. */
	@Column(name="seller_company_name")
	private String sellerCompanyName;
	
	/** The seller company logo id. */
	@OneToOne
	@JoinColumn(name="image_id")
	private ImageEntity sellerCompanyLogoId;
	
	/** The seller property count. */
	@NotNull
	@Column(name="seller_property_count")
	private BigInteger sellerPropertyCount;

	/**
	 * Gets the seller id.
	 *
	 * @return the seller id
	 */
	public BigInteger getSellerId() {
		return sellerId;
	}

	/**
	 * Sets the seller id.
	 *
	 * @param sellerId the new seller id
	 */
	public void setSellerId(BigInteger sellerId) {
		this.sellerId = sellerId;
	}

	/**
	 * Gets the seller property id.
	 *
	 * @return the seller property id
	 */
	public List<PropertyEntity> getSellerPropertyId() {
		return sellerPropertyIds;
	}

	/**
	 * Sets the seller property id.
	 *
	 * @param sellerPropertyIds the new seller property id
	 */
	public void setSellerPropertyId(List<PropertyEntity> sellerPropertyIds) {
		this.sellerPropertyIds = sellerPropertyIds;
	}

	/**
	 * Gets the seller right by.
	 *
	 * @return the seller right by
	 */
	public BigInteger getSellerRightBy() {
		return sellerRightBy;
	}

	/**
	 * Sets the seller right by.
	 *
	 * @param sellerRightBy the new seller right by
	 */
	public void setSellerRightBy(BigInteger sellerRightBy) {
		this.sellerRightBy = sellerRightBy;
	}

	/**
	 * Checks if is seller is active.
	 *
	 * @return true, if is seller is active
	 */
	public boolean isSellerIsActive() {
		return sellerIsActive;
	}

	/**
	 * Sets the seller is active.
	 *
	 * @param sellerIsActive the new seller is active
	 */
	public void setSellerIsActive(boolean sellerIsActive) {
		this.sellerIsActive = sellerIsActive;
	}

	/**
	 * Gets the seller notification id.
	 *
	 * @return the seller notification id
	 */
	public List<NotificationEntity> getSellerNotificationId() {
		return sellerNotificationIds;
	}

	/**
	 * Sets the seller notification id.
	 *
	 * @param sellerNotificationIds the new seller notification id
	 */
	public void setSellerNotificationId(List<NotificationEntity> sellerNotificationIds) {
		this.sellerNotificationIds = sellerNotificationIds;
	}

	/**
	 * Gets the seller feedback id.
	 *
	 * @return the seller feedback id
	 */
	public List<FeedbackEntity> getSellerFeedbackId() {
		return sellerFeedbackIds;
	}

	/**
	 * Sets the seller feedback id.
	 *
	 * @param sellerFeedbackIds the new seller feedback id
	 */
	public void setSellerFeedbackId(List<FeedbackEntity> sellerFeedbackIds) {
		this.sellerFeedbackIds = sellerFeedbackIds;
	}

	/**
	 * Gets the seller created time.
	 *
	 * @return the seller created time
	 */
	public LocalDateTime getSellerCreatedTime() {
		return sellerCreatedTime;
	}

	/**
	 * Sets the seller created time.
	 *
	 * @param sellerCreatedTime the new seller created time
	 */
	public void setSellerCreatedTime(LocalDateTime sellerCreatedTime) {
		this.sellerCreatedTime = sellerCreatedTime;
	}

	/**
	 * Gets the seller private contact.
	 *
	 * @return the seller private contact
	 */
	public BigInteger getSellerPrivateContact() {
		return sellerPrivateContact;
	}

	/**
	 * Sets the seller private contact.
	 *
	 * @param sellerPrivateContact the new seller private contact
	 */
	public void setSellerPrivateContact(BigInteger sellerPrivateContact) {
		this.sellerPrivateContact = sellerPrivateContact;
	}

	/**
	 * Gets the seller company name.
	 *
	 * @return the seller company name
	 */
	public String getSellerCompanyName() {
		return sellerCompanyName;
	}

	/**
	 * Sets the seller company name.
	 *
	 * @param sellerCompanyName the new seller company name
	 */
	public void setSellerCompanyName(String sellerCompanyName) {
		this.sellerCompanyName = sellerCompanyName;
	}

	/**
	 * Gets the seller company logo id.
	 *
	 * @return the seller company logo id
	 */
	public ImageEntity getSellerCompanyLogoId() {
		return sellerCompanyLogoId;
	}

	/**
	 * Sets the seller company logo id.
	 *
	 * @param sellerCompanyLogoId the new seller company logo id
	 */
	public void setSellerCompanyLogoId(ImageEntity sellerCompanyLogoId) {
		this.sellerCompanyLogoId = sellerCompanyLogoId;
	}

	/**
	 * Gets the seller property count.
	 *
	 * @return the seller property count
	 */
	public BigInteger getSellerPropertyCount() {
		return sellerPropertyCount;
	}

	/**
	 * Sets the seller property count.
	 *
	 * @param sellerPropertyCount the new seller property count
	 */
	public void setSellerPropertyCount(BigInteger sellerPropertyCount) {
		this.sellerPropertyCount = sellerPropertyCount;
	}
	
	

}
