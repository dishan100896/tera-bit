package com.project.terabit.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.terabit.entity.UserEntity;
import com.project.terabit.model.User;
import com.project.terabit.repositroy.UserRepository;
import com.project.terabit.service.SignUpUserService;



/**
 * The Class SignUpController.
 */
@RestController
@RequestMapping(value="terabit/api/v1/user")
public class SignUpController {
	
	/** The log. */
	Logger log=LoggerFactory.getLogger(this.getClass());
	
	/** The service. */
	@Autowired
	private SignUpUserService signupservice;
	
	/** The user repository. */
	@Autowired
	private UserRepository userRepository;
	
	/** The prop. */
	Properties prop = new Properties(); 
	
	/** The input stream. */
	InputStream inputStream = getClass().getClassLoader().getResourceAsStream("application.properties");
	
	/** The Constant CONTROLLEREXCEPTION_URLMISSMATCH. */
	private static final String CONTROLLEREXCEPTION_URLMISSMATCH = "CONTROLLER.saltstringmissmatch";
	
	private static final String CONTROLLEREXCEPTION_NOUSER = "CONTROLLER.nouserexist";
	
	/**
	 * Creates the user.
	 *
	 * @param saltstring the saltstring
	 * @param user the user
	 * @return the string
	 * @throws IOException 
	 * @throws Exception the exception
	 */
	@PostMapping(value="/save/{saltstring}")
	public User saveUser(@PathVariable(name="saltstring", required=false) String saltstring, @RequestBody User user) throws ServiceException,ControllerException,IOException {
		User newUser=new User();
		String response="";
		try {
			if(user.getUserId()==null && saltstring==null) {
			newUser=signupservice.saveUser(user);
			}
			UserEntity userentity=userRepository.findUserByUserId(user.getUserId());
			if(userentity==null) {
				throw new ControllerException(CONTROLLEREXCEPTION_NOUSER);
			}
			if(userentity.getSaltString().equals(saltstring) && user.getUserId()!=null) {
				newUser=signupservice.updateUser(user);
			}
			else {
			throw new ControllerException(CONTROLLEREXCEPTION_URLMISSMATCH);
			}
		}catch(ControllerException|ServiceException exception) {
			log.error(exception.getMessage());
		}catch(Exception exception) {
			response="ERROR : ";
			prop.load(inputStream); 
			newUser.setMessage(response+prop.getProperty(exception.getMessage()));
			String s=response+prop.getProperty(exception.getMessage());
			log.error(s);
		}
		return newUser;
	}
	
}
