package com.project.terabit.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.terabit.entity.UserEntity;
import com.project.terabit.model.User;
import com.project.terabit.repositroy.UserRepository;
import com.project.terabit.service.DeleteUserServiceImpl;


/**
 * The Class DeleteController.
 */
@RestController
@RequestMapping(value="terabit/api/v1/user")
public class DeleteController {
	
	   /** The log. */
   	Logger log=LoggerFactory.getLogger(this.getClass());
       
       /** The prop. */
       Properties prop = new Properties(); 
       
       /** The input stream. */
       InputStream inputStream = getClass().getClassLoader().getResourceAsStream("application.properties");
       
       /** The Constant CONTROLLEREXCEPTION_SALTSTRINGMISSMATCH. */
       public static final String CONTROLLEREXCEPTION_SALTSTRINGMISSMATCH="deletecontroller.invalidSignUp";
       
       /** The delete service. */
       @Autowired(required=true)
       private DeleteUserServiceImpl deleteService;
       
       /** The user repository. */
       @Autowired
   	   private UserRepository userRepository;
       
       /**
        * Delete user.
        *
        * @param saltstring the saltstring
        * @param user the user
        * @return the user
        * @throws ControllerException the controller exception
        * @throws IOException Signals that an I/O exception has occurred.
        */
       @PostMapping(value="/delete/{saltstring}")
       public User deleteUser(@PathVariable(name="saltstring", required=true) String saltstring, @RequestBody User user) throws ControllerException,IOException {
    	   		
              User newUser=new User();
              try {
            	  UserEntity userentity=userRepository.findUserByUserId(user.getUserId());
            	  if(userentity.getSaltString().equals(saltstring)) {
                     newUser = deleteService.deleteUser(user);
            	  }else {
            		  throw new ControllerException(CONTROLLEREXCEPTION_SALTSTRINGMISSMATCH);
            	  }
              }
              catch(Exception exception){
                     String response="ERROR : ";
                     prop.load(inputStream); 
                     newUser.setMessage(response+prop.getProperty(exception.getMessage()));
                     String s=response+prop.getProperty(exception.getMessage());
                     log.error(s);      
              }return newUser;
       }
}

