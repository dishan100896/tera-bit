package com.project.terabit.model;

import java.math.BigInteger;
import java.time.LocalDateTime;



/**
 * The Class Cart.
 */
public class Cart {

	/** The cart id. */
	private BigInteger cartId;

	/** The cart property id. */
	private BigInteger cartPropertyId;

	/** The cart user id. */
	private BigInteger cartUserId;

	/** The cart created time. */
	private LocalDateTime cartCreatedTime;

	/** The cart is active. */
	private boolean cartIsActive=false;

	/**
	 * Gets the cart id.
	 *
	 * @return the cart id
	 */
	public BigInteger getCartId() {
		return cartId;
	}

	/**
	 * Sets the cart id.
	 *
	 * @param cartId the new cart id
	 */
	public void setCartId(BigInteger cartId) {
		this.cartId = cartId;
	}

	/**
	 * Gets the cart property id.
	 *
	 * @return the cart property id
	 */
	public BigInteger getCartPropertyId() {
		return cartPropertyId;
	}

	/**
	 * Sets the cart property id.
	 *
	 * @param cartPropertyId the new cart property id
	 */
	public void setCartPropertyId(BigInteger cartPropertyId) {
		this.cartPropertyId = cartPropertyId;
	}

	/**
	 * Gets the cart user id.
	 *
	 * @return the cart user id
	 */
	public BigInteger getCartUserId() {
		return cartUserId;
	}

	/**
	 * Sets the cart user id.
	 *
	 * @param cartUserId the new cart user id
	 */
	public void setCartUserId(BigInteger cartUserId) {
		this.cartUserId = cartUserId;
	}

	/**
	 * Gets the cart created time.
	 *
	 * @return the cart created time
	 */
	public LocalDateTime getCartCreatedTime() {
		return cartCreatedTime;
	}

	/**
	 * Sets the cart created time.
	 *
	 * @param cartCreatedTime the new cart created time
	 */
	public void setCartCreatedTime(LocalDateTime cartCreatedTime) {
		this.cartCreatedTime = cartCreatedTime;
	}

	/**
	 * Checks if is cart is active.
	 *
	 * @return true, if is cart is active
	 */
	public boolean isCartIsActive() {
		return cartIsActive;
	}

	/**
	 * Sets the cart is active.
	 *
	 * @param cartIsActive the new cart is active
	 */
	public void setCartIsActive(boolean cartIsActive) {
		this.cartIsActive = cartIsActive;
	}


}
