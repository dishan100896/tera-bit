package com.project.terabit.service;

import java.util.UUID;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.project.terabit.TerabitApplicationTests;
import com.project.terabit.entity.UserEntity;
import com.project.terabit.model.User;
import com.project.terabit.repositroy.UserRepository;

public class DeleteUserServiceTest extends TerabitApplicationTests{

	@Mock 
	UserRepository userRepository;
	
	@InjectMocks
	DeleteUserServiceImpl service;

	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void deleteUserTest() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("DELETEUSERSERVICE.invalid_user");
		String uuid = "8705b317-3bda-4ef7-abf7-2d951edc4ee7";
		User user = new User();
		user.setUserId(UUID.fromString(uuid));
		user.setUserIsActive(true);
		Mockito.when(userRepository.findUserByUserId(UUID.fromString(uuid))).thenReturn(null);
		service.deleteUser(user);	
	}

}
